#include "Particle.h"

#define OVR_OS_MAC
#include "OVR_CAPI.h"
#include "OVR_CAPI_GL.h"
#include "Extras/OVR_Math.h"
#include <SDL2/SDL_syswm.h>

#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <time.h>
#include <stdio.h>
#include <assert.h>
#define PLISTSIZE 1000
using namespace OVR;

float lastTime = 0;
float curTime = 0;
float difTime = 0;
int vNum = 0;

float camx = -15.0f, camy = 40.0f, camz = 0.0f;
float dirx = 2.0f/sqrt(29), diry = -5.0f/sqrt(29), dirz = 0.0f;
float rdirx = 0.0f, rdiry = 0.0f, rdirz = 1.0f;
int windowWidth = 800, windowHeight = 600;
float mSense = 60.0f;
int pointSize = 8;
std::vector<glm::mat4> modelPos;
float maxMag = 1.0f;

float eyeDist = 0.2f;
glm::vec3 eyeOffset[2] = { glm::vec3(-rdirx*eyeDist, -rdiry*eyeDist, -rdirz*eyeDist), 
                           glm::vec3(rdirx*eyeDist, rdiry*eyeDist, rdirz*eyeDist) };

//Vector structure
glm::vec3 bmin(-25.0f, -25.0f, -25.0f);
glm::vec3 bmax(25.0f, 25.0f, 25.0f);
glm::vec3 resolution = glm::vec3(0.5f, 0.5f, 0.5f);
glm::vec3 fieldSize = glm::abs(bmax - bmin);
glm::vec3 maxIndexes;

bool DEBUG_ON = true;
bool timeDep = false;
float flowTime = 0.0f;
float startTime = 0.0f;

GLuint InitShader(const char* vShaderFileName, const char* fShaderFileName);
bool fullscreen = false;
void Win2PPM(int width, int height);
glm::vec3 getForceVector(const glm::vec3& position, glm::vec3*** field, glm::vec3 step, const glm::vec3& min);
glm::vec3 getSectionVector(const glm::vec3& position);
bool outsideBounds(const glm::vec3& pos, const glm::vec3& min, const glm::vec3& max);
void initVecField(glm::vec3*** field, int& sizex, int& sizey, int& sizez);

/**
 * From http://www.csc.kth.se/~weinkauf/notes/amiramesh.html
 * Reads in a vector field in the Amira file format
 */
const char* FindAndJump(const char* buffer, const char* SearchString);
void readField(const char* FileName, glm::vec3**** field);

void quicksort(std::vector<Particle>& parts, const glm::vec3 cpos, int low, int high);
int partition(std::vector<Particle>& p, int low, int high, const glm::vec3 cpos);
float distSq(const glm::vec3 cpos, const Particle& p);

int main(int argc, char** argv) {
    SDL_Init(SDL_INIT_VIDEO);  //Initialize Graphics (for OpenGL)
    
    //Ask SDL to get a recent version of OpenGL (3.2 or greater)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    int x = SDL_WINDOWPOS_CENTERED;
    int y = SDL_WINDOWPOS_CENTERED;
    Uint32 flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

    bool debug = false;

    ovr_Initialize();
    ovrHmd hmd = ovrHmd_Create(0);

    if (hmd == NULL) {
            hmd = ovrHmd_CreateDebug(ovrHmd_DK1);
            debug = true;
    }

    if (debug == false && hmd->HmdCaps & ovrHmdCap_ExtendDesktop) {
            x = hmd->WindowsPos.x;
            y = hmd->WindowsPos.y;
            flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    }

    int w = hmd->Resolution.w;
    int h = hmd->Resolution.h;
	
	//Create a window (offsetx, offsety, width, height, flags)
	SDL_Window* window = SDL_CreateWindow("Oculus Rift Particle Vector Field", x, y, w, h, flags);
	
	//Create a context to draw in
	SDL_GLContext context = SDL_GL_CreateContext(window);
	
    //SDL_SetRelativeMouseMode(SDL_TRUE);
    
	//GLEW loads new OpenGL functions
	glewExperimental = GL_TRUE; //Use the new way of testing which methods are supported
	glewInit();

    Sizei recommendedTex0Size = ovrHmd_GetFovTextureSize(hmd, ovrEye_Left, hmd->DefaultEyeFov[0], 1.0f);
    Sizei recommendedTex1Size = ovrHmd_GetFovTextureSize(hmd, ovrEye_Right, hmd->DefaultEyeFov[1], 1.0f);
    Sizei renderTargetSize;
    renderTargetSize.w = recommendedTex0Size.w + recommendedTex1Size.w;
    renderTargetSize.h = std::max(recommendedTex0Size.h, recommendedTex1Size.h);

    GLuint frameBuffer;
    glGenFramebuffers(1, &frameBuffer);

    GLuint texture[2];
    glGenTextures(1, &texture[0]);
    glGenTextures(1, &texture[1]);

    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, renderTargetSize.w, renderTargetSize.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture[0], 0);

    glBindTexture(GL_TEXTURE_2D, texture[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, renderTargetSize.w, renderTargetSize.h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, texture[1], 0);

    GLenum DrawBuffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, DrawBuffers);

    GLuint renderBuffer;
    glGenRenderbuffers(1, &renderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, renderTargetSize.w, renderTargetSize.h);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBuffer);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            glDeleteFramebuffers(1, &frameBuffer);
            glDeleteTextures(1, &texture[0]);
            glDeleteTextures(1, &texture[1]);
            glDeleteRenderbuffers(1, &renderBuffer);

            SDL_GL_DeleteContext(context);
            SDL_DestroyWindow(window);
            ovrHmd_Destroy(hmd);
            ovr_Shutdown();
            SDL_Quit();
            return 0;
    }

    ovrFovPort eyeFov[2] = { hmd->DefaultEyeFov[0], hmd->DefaultEyeFov[1] };

    ovrRecti eyeRenderViewport[2];
    eyeRenderViewport[0].Pos = Vector2i(0, 0);
    eyeRenderViewport[0].Size = Sizei(renderTargetSize.w / 2, renderTargetSize.h);
    eyeRenderViewport[1].Pos = Vector2i((renderTargetSize.w + 1) / 2, 0);
    eyeRenderViewport[1].Size = eyeRenderViewport[0].Size;

    ovrGLTexture eyeTexture[2];
    eyeTexture[0].OGL.Header.API = ovrRenderAPI_OpenGL;
    eyeTexture[0].OGL.Header.TextureSize = renderTargetSize;
    eyeTexture[0].OGL.Header.RenderViewport = eyeRenderViewport[0];
    eyeTexture[0].OGL.TexId = texture[0];

    eyeTexture[1] = eyeTexture[0];
    eyeTexture[1].OGL.TexId = texture[1];
    eyeTexture[1].OGL.Header.RenderViewport = eyeRenderViewport[1];

    SDL_SysWMinfo info;
    SDL_VERSION(&info.version);
    SDL_GetWindowWMInfo(window, &info);

    ovrGLConfig cfg;
    cfg.OGL.Header.API = ovrRenderAPI_OpenGL;
    cfg.OGL.Header.BackBufferSize = Sizei(hmd->Resolution.w, hmd->Resolution.h);
    cfg.OGL.Header.Multisample = 1; 

    ovrEyeRenderDesc eyeRenderDesc[2];
    ovrHmd_ConfigureRendering(hmd, &cfg.Config, /*ovrDistortionCap_Chromatic | */ovrDistortionCap_Vignette | ovrDistortionCap_TimeWarp | ovrDistortionCap_Overdrive, eyeFov, eyeRenderDesc);
    ovrHmd_SetEnabledCaps(hmd, ovrHmdCap_LowPersistence | ovrHmdCap_DynamicPrediction);
    ovrHmd_ConfigureTracking(hmd, ovrTrackingCap_Orientation | ovrTrackingCap_MagYawCorrection | ovrTrackingCap_Position, 0);
    
    int shaderProgram = InitShader("vertex.glsl", "fragment.glsl");
    glUseProgram(shaderProgram); //Set the active shader (only one can be used at a time)
    printf("Shader Compiled\n");


    GLuint vao;
	glGenVertexArrays(1, &vao); //Create a VAO
	glBindVertexArray(vao); //Bind the above created VAO to the current context
    
    srand(time(0));
    
    float partData[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.5f, 0.5f};
    GLuint vbo[1];
	glGenBuffers(1, vbo);  //Create 1 buffer called vbo
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Set the vbo as the active array buffer (Only one buffer can be active at a time)
	glBufferData(GL_ARRAY_BUFFER, 8*sizeof(float), partData, GL_STATIC_DRAW); //upload vertices to vbo
        
    //Emitter plane
    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), 0);
	  //Attribute, vals/attrib., type, normalized?, stride, offset
	  //Binds to VBO current GL_ARRAY_BUFFER 
	glEnableVertexAttribArray(posAttrib);
    
    GLint normAttrib = glGetAttribLocation(shaderProgram, "inNormal");
	glVertexAttribPointer(normAttrib, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(5*sizeof(float)));
	glEnableVertexAttribArray(normAttrib);
    
    //glEnable(GL_DEPTH_TEST);
    SDL_Event windowEvent;
    //glPointSize(pointSize);
    GLint uniPSize = glGetUniformLocation(shaderProgram, "pSize");
    glUniform1i(uniPSize, pointSize);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    
    std::vector<Particle> plist;
    Particle p;
    for(int i = 0; i < PLISTSIZE; i++) {
        glm::vec3 randpos;
        if(!timeDep) {
            randpos = glm::vec3((float)rand()/RAND_MAX*fieldSize.x+bmin.x, 
                                      (float)rand()/RAND_MAX*fieldSize.y+bmin.y, 
                                      (float)rand()/RAND_MAX*fieldSize.z+bmin.z);
        }
        else {
            randpos = glm::vec3((float)rand()/RAND_MAX*fieldSize.x+bmin.x, 0, 
                                      (float)rand()/RAND_MAX*fieldSize.z+bmin.z);
        }
        p = Particle(randpos, glm::vec3(0), glm::vec3(0.0f, 0.0f, 1.0f));
        p.setMass(1.0f);
        plist.push_back(p);
    }
    
    glm::vec3*** vecfield = 0;
    if(argc == 2) {
        std::string filename(argv[1]);
        if(filename.find(".am") != std::string::npos) {
            readField(argv[1], &vecfield);
            printf("Size: (%f %f %f)\n", fieldSize.x, fieldSize.y, fieldSize.z);
        }
        else {
            printf("Unsupported File Format: %s\n", filename.c_str());
            std::exit(1);
        }
    }
    else {
        int vsx, vsy, vsz;
        vsx = round((bmax.x-bmin.x) / resolution.x);
        vsy = round((bmax.y-bmin.y) / resolution.y);
        vsz = round((bmax.z-bmin.z) / resolution.z);
        printf("Size: (%d, %d, %d)\n", vsx, vsy, vsz);
        maxIndexes = glm::vec3(vsx, vsy, vsz);
        vecfield = new glm::vec3**[vsx];
        for(int i = 0; i < vsx; i++) {
            vecfield[i] = new glm::vec3*[vsy];
            for(int j = 0; j < vsy; j++) {
                vecfield[i][j] = new glm::vec3[vsz];
                for(int k = 0; k < vsz; k++) {
                    vecfield[i][j][k] = getSectionVector(glm::vec3(bmin.x+(i*resolution.x), bmin.y+(j*resolution.y), bmin.z+(k*resolution.z)));
                }
            }
        }
    }
    if(vecfield == 0) {
        printf("NULL\n");
    }
    SDL_SetWindowTitle(window, "FPS: 60    0 Particles");
    int numFrames = 0;
    
    lastTime = SDL_GetTicks()/1000.f;
    curTime = lastTime;
    difTime = 0;
    float fpsTime = 0;
    //Map of keys to states, with 0 and 1 being left and right mouse button
    std::map<char, bool> kmap;
    bool quit = false;
    bool usegrav = false;
    bool vfield = true;
    bool run = false;
    GLint uniColor = glGetUniformLocation(shaderProgram, "inColor");
    GLint uniModel = glGetUniformLocation(shaderProgram, "model");
    GLint uniProj = glGetUniformLocation(shaderProgram, "proj");
    GLint uniView = glGetUniformLocation(shaderProgram, "view");
    GLint uniTivm = glGetUniformLocation(shaderProgram, "tivm");
    float pMaxMag = 1.0f;
    ovrHmd_DismissHSWDisplay(hmd);
    while(true) {
        while(SDL_PollEvent(&windowEvent)) {
            if (windowEvent.type == SDL_QUIT) {
                quit = true;
                break;
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_ESCAPE) {
                #ifdef DEBUG
                printf("Escape\n");
                #endif
                quit = true;
                break;
            }
            else if(windowEvent.type == SDL_MOUSEBUTTONDOWN && windowEvent.button.button == SDL_BUTTON_LEFT) {
                //Shift - translate position
                const Uint8 *keystate = SDL_GetKeyboardState(NULL);
                if(keystate[SDL_SCANCODE_LSHIFT]) {
                    kmap[2] = true;
                    kmap[0] = false;
                }
                else {
                    kmap[0] = true;
                    kmap[2] = false;
                }
            }
            else if(windowEvent.type == SDL_MOUSEBUTTONUP && windowEvent.button.button == SDL_BUTTON_LEFT) {
                const Uint8 *keystate = SDL_GetKeyboardState(NULL);
                if(keystate[SDL_SCANCODE_LSHIFT]) {
                    kmap[2] = false;
                    kmap[0] = false;
                }
                else {
                    kmap[0] = false;
                    kmap[2] = false;
                }
            }
            else if(windowEvent.type == SDL_MOUSEBUTTONDOWN && windowEvent.button.button == SDL_BUTTON_RIGHT) {
                if(windowEvent.key.keysym.mod & KMOD_SHIFT) {
                    kmap[3] = true;
                    kmap[1] = false;
                }
                else {
                    kmap[1] = true;
                    kmap[3] = false;
                }
            }
            else if(windowEvent.type == SDL_MOUSEBUTTONUP && windowEvent.button.button == SDL_BUTTON_RIGHT) {
                if(windowEvent.key.keysym.mod & KMOD_SHIFT) {
                    kmap[3] = false;
                    kmap[1] = false;
                }
                else {
                    kmap[1] = false;
                    kmap[3] = false;
                }
            }
            else if(windowEvent.type == SDL_MOUSEMOTION && kmap[0]) { //rotate around point
                int xdist = windowEvent.motion.xrel;
                int ydist = windowEvent.motion.yrel;
                float xtheta = atan2f((float)xdist, 1.0f);
                float ytheta = atan2f((float)ydist, 1.0f);
                //printf("Theta: %f\n", -theta/25.0f);
                glm::mat4 rot = glm::rotate(glm::mat4(1.0f), -xtheta/mSense, glm::vec3(0, 1, 0));
                rot = glm::rotate(rot, -ytheta/mSense, glm::vec3(0, 0, 1));
                glm::vec4 newDir = rot * glm::vec4(dirx, diry, dirz, 1.0f);
                glm::vec4 rnewDir = rot * glm::vec4(rdirx, rdiry, rdirz, 1.0f);
                dirx = newDir.x;
                diry = newDir.y;
                dirz = newDir.z;
                rdirx = rnewDir.x;
                rdiry = rnewDir.y;
                rdirz = rnewDir.z;
            }
            else if(windowEvent.type == SDL_MOUSEMOTION && kmap[2]) { //translate point
                int xdist = windowEvent.motion.xrel;
                int ydist = windowEvent.motion.yrel;
                float scale = 0.2f;
                glm::vec3 forward = glm::vec3(dirx, diry, dirz);
                glm::vec3 right = glm::vec3(rdirx, rdiry, rdirz);
                glm::vec3 newpos = glm::vec3(camx, camy, camz);
                newpos += right * (float)(xdist * scale);
                glm::vec3 up = glm::cross(forward, right);
                newpos += up * (float)(ydist * scale);
                camx = newpos.x;
                camy = newpos.y;
                camz = newpos.z;
            }
            else if(windowEvent.type == SDL_MOUSEWHEEL) { //Scroll
                int ydist = windowEvent.wheel.y;
                float scale = 0.5f;
                glm::vec3 forward = glm::vec3(dirx, diry, dirz);
                glm::vec3 newpos = glm::vec3(camx, camy, camz);
                newpos += forward * (float)(ydist * scale);
                camx = newpos.x;
                camy = newpos.y;
                camz = newpos.z;
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_EQUALS) { //PointSize
                ++pointSize;
                glUniform1i(uniPSize, pointSize);
                printf("Point Size: %d\n", pointSize);
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_MINUS) { //PointSize
                --pointSize;
                if(pointSize < 1) {
                    pointSize = 1;
                }
                glUniform1i(uniPSize, pointSize);
                printf("Point Size: %d\n", pointSize);
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_g) { //Gravity
                usegrav = !usegrav;
                if(usegrav) {
                    printf("Gravity On\n");
                }
                else {
                    printf("Gravity Off\n");
                }
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_w) { //Vector Field
                vfield = !vfield;
                if(vfield) {
                    printf("Vector Field On\n");
                }
                else {
                    printf("Vector Field Off\n");
                }
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_p) { //Pause
                run = !run;
                if(run) {
                    printf("Unpaused\n");
                }
                else {
                    printf("Paused\n");
                }
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_r) { //Reset
                run = false;
                plist.clear();
                for(int i = 0; i < PLISTSIZE; i++) {
                    glm::vec3 randpos;
                    if(!timeDep) {
                        randpos = glm::vec3((float)rand()/RAND_MAX*fieldSize.x+bmin.x,
                                            (float)rand()/RAND_MAX*fieldSize.y+bmin.y,
                                            (float)rand()/RAND_MAX*fieldSize.z+bmin.z);
                    }
                    else {
                        randpos = glm::vec3((float)rand()/RAND_MAX*fieldSize.x+bmin.x, 0,
                                            (float)rand()/RAND_MAX*fieldSize.z+bmin.z);
                    }
                    p = Particle(randpos, glm::vec3(0), glm::vec3(0.0f, 0.0f, 1.0f));
                    p.setMass(1.0f);
                    plist.push_back(p);
                }
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_x) { //Increase eye dist
                eyeDist += 0.1f;
                if(eyeDist > 1.0f) {
                    eyeDist = 1.0f;
                }
                eyeOffset[0] = glm::vec3(-rdirx*eyeDist, -rdiry*eyeDist, -rdirz*eyeDist); 
                eyeOffset[1] = glm::vec3(rdirx*eyeDist, rdiry*eyeDist, rdirz*eyeDist);
            }
            else if(windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_z) { //Decrease eye dist
                eyeDist -= 0.1f;
                if(eyeDist < 0.1f) {
                    eyeDist = 0.1f;
                }
                eyeOffset[0] = glm::vec3(-rdirx*eyeDist, -rdiry*eyeDist, -rdirz*eyeDist); 
                eyeOffset[1] = glm::vec3(rdirx*eyeDist, rdiry*eyeDist, rdirz*eyeDist);
            }
        }
        if(quit) {
            break;
        }

        ovrHmd_BeginFrame(hmd, 0);
        ovrVector3f hmdToEyeViewOffset[2] = { eyeRenderDesc[0].HmdToEyeViewOffset, eyeRenderDesc[1].HmdToEyeViewOffset };
        ovrPosef eyeRenderPose[2];
        ovrHmd_GetEyePoses(hmd, 0, hmdToEyeViewOffset, eyeRenderPose, NULL);
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

        // Clear the screen to default color
        glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        #ifdef DEBUG
        printf("Update\n");
        #endif        
        
        glm::mat4 vm;
        glm::mat4 model;
        glm::mat4 proj;   
        
        glm::vec3 color;
        glm::vec3 pos;
        Particle* pp;
        //#pragma omp parallel for schedule(static, 200)
        if(run) {
            quicksort(plist, glm::vec3(camx, camy, camz), 0, plist.size()-1);
        }
        for (int eyeIndex = 0; eyeIndex < ovrEye_Count; eyeIndex++) {
            glBindTexture(GL_TEXTURE_2D, texture[eyeIndex]);
            for(int i = 0; i < (signed)plist.size(); i++) {
                //ovrEyeType eye = hmd->EyeRenderOrder[eyeIndex];
                //Matrix4f MVPMatrix = Matrix4f(ovrMatrix4f_Projection(eyeRenderDesc[eye].Fov, 0.01f, 10000.0f, true)) * Matrix4f(Quatf(eyeRenderPose[eye].Orientation).Inverted()) * Matrix4f::Translation(-Vector3f(eyeRenderPose[eye].Position));
                //glUniformMatrix4fv(MVPMatrixLocation, 1, GL_FALSE, &MVPMatrix.Transposed().M[0][0]);
                //glViewport(eyeRenderViewport[eye].Pos.x, eyeRenderViewport[eye].Pos.y, eyeRenderViewport[eye].Size.w, eyeRenderViewport[eye].Size.h);
                //glDrawArrays(GL_TRIANGLES, 0, 3);
                pp = &plist[i];
                color = pp->getColor();
                pos = pp->getPosition();
                glUniform3f(uniColor, color.x, color.y, color.z);
                
                model = glm::mat4();
                model = glm::translate(model, pos);
                glUniformMatrix4fv(uniModel, 1, GL_FALSE, glm::value_ptr(model));
                glm::mat4 view = glm::lookAt(
                    glm::vec3(camx+eyeOffset[eyeIndex].x, camy+eyeOffset[eyeIndex].y, camz+eyeOffset[eyeIndex].z),  //Cam Position
                    glm::vec3(camx+eyeOffset[eyeIndex].x+dirx, camy+eyeOffset[eyeIndex].y+diry, camz+eyeOffset[eyeIndex].z+dirz),  //Look at point
                    glm::vec3(0.0f, 1.0f, 0.0f)); //Up
                glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));
                proj = glm::perspective(2.356194f, 800.0f / 600.0f, 0.1f, 100.0f); //FOV, aspect, near, far
                glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(proj)); 
                vm = view * model;
                vm = glm::inverse(vm);
                glUniformMatrix4fv(uniTivm, 1, GL_TRUE, glm::value_ptr(vm));
                glDrawArrays(GL_POINTS, 0, 1);
            }
        }
        glBindVertexArray(0);
        ovrTexture etex[2] = { eyeTexture[0].Texture, eyeTexture[1].Texture };
        ovrHmd_EndFrame(hmd, eyeRenderPose, etex);
        glBindVertexArray(vao);
        for(int i = 0; i < (signed)plist.size(); i++) {
            if(run) {
                if(vfield) {
                    glm::vec3 f = getForceVector(pos, vecfield, resolution, bmin);
                    pp->update(0, 0, modelPos, 0, usegrav, f);
                    float mag = glm::length(pp->getVelocity());
                    if(mag > maxMag) {
                        maxMag = mag;
                    }
                }
                else {
                    pp->update(0, 0, modelPos, 0, usegrav);
                }
            }
            if(outsideBounds(pp->getPosition(), bmin, bmax)) {
                pp->setPosition(glm::vec3((float)rand()/RAND_MAX*fieldSize.x+bmin.x, 
                                          (float)rand()/RAND_MAX*fieldSize.y+bmin.y, 
                                          (float)rand()/RAND_MAX*fieldSize.z+bmin.z));
                pp->setAcceleration(getForceVector(pp->getPosition(), vecfield, resolution, bmin) * pp->getMass());
                pp->setVelocity(pp->getAcceleration() * difTime);
                float mag = glm::length(pp->getVelocity());
                glm::vec3 col;
                col.x = glm::clamp(mag / (0.95f*maxMag), 0.0f, 1.0f);
                col.y = 0.0f;
                col.z = glm::clamp(((0.95f*maxMag) - mag) / (0.95f*maxMag), 0.0f, 1.0f);
                pp->setColor(col);
            }
        }
        if(maxMag > pMaxMag) {
            printf("Max Magnitude: %f\n", maxMag);
            pMaxMag = maxMag;
        }
        lastTime = curTime;
        curTime = SDL_GetTicks()/1000.f;
        difTime = curTime - lastTime;
        
        if(timeDep) {
            flowTime += difTime;
            if(flowTime > bmax.y) {
                flowTime = startTime + (flowTime - bmax.y);
            }
        }
        
        numFrames++;
        fpsTime += difTime;
        if(fpsTime > 1.0f) {
            SDL_SetWindowTitle(window, ("FPS: " + std::to_string(numFrames/fpsTime) + "    " + std::to_string(plist.size()).c_str() + " Particles").c_str());
            fpsTime -= 1.0f;
            numFrames = 0;
        }

        SDL_GL_SwapWindow(window);
    }
    
    glDeleteProgram(shaderProgram);
    glDeleteBuffers(1, vbo);
    glDeleteVertexArrays(1, &vao);

    glDeleteFramebuffers(1, &frameBuffer);
    glDeleteTextures(1, &texture[0]);
    glDeleteTextures(1, &texture[1]);
    glDeleteRenderbuffers(1, &renderBuffer);
    
	//Clean Up
    ovrHmd_Destroy(hmd);
    ovr_Shutdown();
    SDL_DestroyWindow(window);
	SDL_GL_DeleteContext(context);
	SDL_Quit();
    
    return 0;
}

glm::vec3 getSectionVector(const glm::vec3& position) {
    //return glm::vec3(-position.z-position.x, (position.x-position.z)*sin(position.x*position.y*position.z), position.x-position.z);
    //return glm::vec3(sin(sqrt(position.x*position.x+position.z*position.z+position.y*position.y)), sin(sqrt(position.x*position.x+position.z*position.z+position.y*position.y)), sin(sqrt(position.x*position.x+position.z*position.z+position.y*position.y)));
    //return glm::vec3(-position.y*position.z, 0, position.x*position.y);
    return glm::vec3(position.z, sin(M_PI*position.y), -position.x);
    //return glm::vec3(1, sin(position.y), sin(position.x)+cos(position.z));
}

glm::vec3 getForceVector(const glm::vec3& position, glm::vec3*** field, glm::vec3 step, const glm::vec3& min) {
    //Figure out which box the particle is in
    glm::vec3 index;
    index.x = floor((position.x-min.x) / step.x);
    if(timeDep) {
        index.y = floor((flowTime-startTime) / step.z);
    }
    else {
        index.y = floor((position.y-min.y) / step.y);
    }
    index.z = floor((position.z-min.z) / step.z);
    /*
    if((int)round(index.x) < 0 || (int)round(index.x) >= (int)maxIndexes.x) {
        printf("Pos: (%f, %f, %f)\n", position.x, position.y, position.z);
        printf("Step: %f\n", step);
        printf("Min: (%f, %f, %f)\n", min.x, min.y, min.z);
        printf("Max: (%f, %f, %f)\n", bmax.x, bmax.y, bmax.z);
        printf("Max Index: (%f, %f, %f)\n", maxIndexes.x, maxIndexes.y, maxIndexes.z);
        printf("Index: (%d, %d, %d)\n", (int)round(index.x), (int)round(index.y), (int)round(index.z));
    }
    if((int)round(index.y) < 0 || (int)round(index.y) >= (int)maxIndexes.y) {
        printf("Pos: (%f, %f, %f)\n", position.x, position.y, position.z);
        printf("Step: %f\n", step);
        printf("Min: (%f, %f, %f)\n", min.x, min.y, min.z);
        printf("Max: (%f, %f, %f)\n", bmax.x, bmax.y, bmax.z);
        printf("Max Index: (%f, %f, %f)\n", maxIndexes.x, maxIndexes.y, maxIndexes.z);
        printf("Index: (%d, %d, %d)\n", (int)round(index.x), (int)round(index.y), (int)round(index.z));
    }
    if((int)round(index.z) < 0 || (int)round(index.z) >= (int)maxIndexes.z) {
        printf("Pos: (%f, %f, %f)\n", position.x, position.y, position.z);
        printf("Step: %f\n", step);
        printf("Min: (%f, %f, %f)\n", min.x, min.y, min.z);
        printf("Max: (%f, %f, %f)\n", bmax.x, bmax.y, bmax.z);
        printf("Max Index: (%f, %f, %f)\n", maxIndexes.x, maxIndexes.y, maxIndexes.z);
        printf("Index: (%d, %d, %d)\n", (int)round(index.x), (int)round(index.y), (int)round(index.z));
    }
    */
    if((int)round(index.x) >= (int)maxIndexes.x) {
        index.x--;
        printf("Fixed X\n");
    }
    if((int)round(index.y) >= (int)maxIndexes.y) {
        index.y--;
        printf("Fixed Y\n");
    }
    if((int)round(index.z) >= (int)maxIndexes.z) {
        index.z--;
        printf("Fixed Z\n");
    }
    //glm::vec3 bcenter = min + (index * step) + (step / 2.0f);
    //Do an interpolation of the vector in that box and the neighboring boxes
    glm::vec3 vcenter = field[(int)round(index.x)][(int)round(index.y)][(int)round(index.z)];
    
    return vcenter;
}

bool outsideBounds(const glm::vec3& pos, const glm::vec3& min, const glm::vec3& max) {
    if(pos.x < min.x || pos.x > max.x || pos.y < min.y || pos.y > max.y || pos.z < min.z || pos.z > max.z) {
        return true;
    }
    return false;
}

void quicksort(std::vector<Particle>& parts, const glm::vec3 cpos, int low, int high) {
    if(low < high) {
        int index = partition(parts, low, high, cpos);
        quicksort(parts, cpos, low, index-1);
        quicksort(parts, cpos, index+1, high);
    }
}

int partition(std::vector<Particle>& p, int low, int high, const glm::vec3 cpos) {
    int rindex = low + (rand() % (high-low+1));
    Particle pivot = p[rindex];
    p[rindex] = p[high];
    p[high] = pivot;
    int storeIndex = low;
    Particle temp;
    float pivDist = distSq(cpos, pivot);
    for(int i = low; i < high; i++) {
        if(distSq(cpos, p[i]) >= pivDist) { //Sorts it high to low so it renders back to front
            temp = p[i];
            p[i] = p[storeIndex];
            p[storeIndex] = temp;
            storeIndex++;
        }
    }
    temp = p[storeIndex];
    p[storeIndex] = p[high];
    p[high] = temp;
    return storeIndex;
}

float distSq(const glm::vec3 cpos, const Particle& p) {
    glm::vec3 pos = p.getPosition();
    glm::vec3 v = pos - cpos;
    return glm::dot(v, v);
}

// Create a NULL-terminated string by reading the provided file
static char* readShaderSource(const char* shaderFile)
{
	FILE *fp;
	long length;
	char *buffer;

	// open the file containing the text of the shader code
	fp = fopen(shaderFile, "r");

	// check for errors in opening the file
	if (fp == NULL) {
		printf("can't open shader source file %s\n", shaderFile);
		return NULL;
	}

	// determine the file size
	fseek(fp, 0, SEEK_END); // move position indicator to the end of the file;
	length = ftell(fp);  // return the value of the current position

	// allocate a buffer with the indicated number of bytes, plus one
	buffer = new char[length + 1];

	// read the appropriate number of bytes from the file
	fseek(fp, 0, SEEK_SET);  // move position indicator to the start of the file
	fread(buffer, 1, length, fp); // read all of the bytes

	// append a NULL character to indicate the end of the string
	buffer[length] = '\0';

	// close the file
	fclose(fp);

	// return the string
	return buffer;
}

// Create a GLSL program object from vertex and fragment shader files
GLuint InitShader(const char* vShaderFileName, const char* fShaderFileName)
{
	GLuint vertex_shader, fragment_shader;
	GLchar *vs_text, *fs_text;
	GLuint program;

	// check GLSL version
	printf("GLSL version: %s\n\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	// Create shader handlers
	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

	// Read source code from shader files
	vs_text = readShaderSource(vShaderFileName);
	fs_text = readShaderSource(fShaderFileName);

	// error check
	if (vs_text == NULL) {
		printf("Failed to read from vertex shader file %s\n", vShaderFileName);
		exit(1);
	} else if (DEBUG_ON) {
		printf("Vertex Shader:\n=====================\n");
		printf("%s\n", vs_text);
		printf("=====================\n\n");
	}
	if (fs_text == NULL) {
		printf("Failed to read from fragent shader file %s\n", fShaderFileName);
		exit(1);
	} else if (DEBUG_ON) {
		printf("\nFragment Shader:\n=====================\n");
		printf("%s\n", fs_text);
		printf("=====================\n\n");
	}

	// Load Vertex Shader
	const char *vv = vs_text;
	glShaderSource(vertex_shader, 1, &vv, NULL);  //Read source
	glCompileShader(vertex_shader); // Compile shaders
	GLint compiled;
	// Check for errors
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		printf("Vertex shader failed to compile:\n");
		if (DEBUG_ON) {
			GLint logMaxSize, logLength;
			glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &logMaxSize);
			printf("printing error message of %d bytes\n", logMaxSize);
			char* logMsg = new char[logMaxSize];
			glGetShaderInfoLog(vertex_shader, logMaxSize, &logLength, logMsg);
			printf("%d bytes retrieved\n", logLength);
			printf("error message: %s\n", logMsg);
			delete[] logMsg;
		}
		exit(1);
	}
	
	// Load Fragment Shader
	const char *ff = fs_text;
	glShaderSource(fragment_shader, 1, &ff, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled);
	
	//Check for Errors
	if (!compiled) {
		printf("Fragment shader failed to compile\n");
		if (DEBUG_ON) {
			GLint logMaxSize, logLength;
			glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &logMaxSize);
			printf("printing error message of %d bytes\n", logMaxSize);
			char* logMsg = new char[logMaxSize];
			glGetShaderInfoLog(fragment_shader, logMaxSize, &logLength, logMsg);
			printf("%d bytes retrieved\n", logLength);
			printf("error message: %s\n", logMsg);
			delete[] logMsg;
		}
		exit(1);
	}

	// Create the program
	program = glCreateProgram();

	// Attach shaders to program
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);

	// Link and set program to use
	glLinkProgram(program);
	glUseProgram(program);

	return program;
}

void Win2PPM(int width, int height){
	char outdir[10] = "out/"; //Must be defined!
	int i,j;
	FILE* fptr;
    static int counter = 0;
    char fname[32];
    unsigned char *image;
    
    /* Allocate our buffer for the image */
    image = (unsigned char *)malloc(3*width*height*sizeof(char));
    if (image == NULL) {
      fprintf(stderr,"ERROR: Failed to allocate memory for image\n");
    }
    
    /* Open the file */
    sprintf(fname,"%simage_%04d.ppm",outdir,counter);
    if ((fptr = fopen(fname,"w")) == NULL) {
      fprintf(stderr,"ERROR: Failed to open file for window capture\n");
    }
    
    /* Copy the image into our buffer */
    glReadBuffer(GL_BACK);
    glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,image);
    
    /* Write the PPM file */
    fprintf(fptr,"P6\n%d %d\n255\n",width,height);
    for (j=height-1;j>=0;j--) {
      for (i=0;i<width;i++) {
         fputc(image[3*j*width+3*i+0],fptr);
         fputc(image[3*j*width+3*i+1],fptr);
         fputc(image[3*j*width+3*i+2],fptr);
      }
    }
    
    free(image);
    fclose(fptr);
    counter++;
}

/** Find a string in the given buffer and return a pointer
    to the contents directly behind the SearchString.
    If not found, return the buffer. A subsequent sscanf()
    will fail then, but at least we return a decent pointer.
*/
const char* FindAndJump(const char* buffer, const char* SearchString)
{
    const char* FoundLoc = strstr(buffer, SearchString);
    if (FoundLoc) return FoundLoc + strlen(SearchString);
    return buffer;
}


/** A simple routine to read an AmiraMesh file
    that defines a scalar/vector field on a uniform grid.
*/
void readField(const char* FileName, glm::vec3**** field)
{
    //const char* FileName = "testscalar.am";
    //const char* FileName = "testvector2c.am";
    //const char* FileName = "testvector3c.am";

    FILE* fp = fopen(FileName, "rb");
    if (!fp)
    {
        printf("Could not find %s\n", FileName);
        return;
    }

    printf("Reading %s\n", FileName);

    //We read the first 2k bytes into memory to parse the header.
    //The fixed buffer size looks a bit like a hack, and it is one, but it gets the job done.
    char buffer[2048];
    fread(buffer, sizeof(char), 2047, fp);
    buffer[2047] = '\0'; //The following string routines prefer null-terminated strings

    if (!strstr(buffer, "# AmiraMesh BINARY-LITTLE-ENDIAN 2.1"))
    {
        printf("Not a proper AmiraMesh file.\n");
        fclose(fp);
        return;
    }

    //Find the Lattice definition, i.e., the dimensions of the uniform grid
    int xDim(0), yDim(0), zDim(0);
    sscanf(FindAndJump(buffer, "define Lattice"), "%d %d %d", &xDim, &yDim, &zDim);
    printf("\tGrid Dimensions: %d %d %d\n", xDim, yDim, zDim);
    *field = new glm::vec3**[xDim];
    for(int i = 0; i < xDim; i++) {
        (*field)[i] = new glm::vec3*[yDim];
        for(int j = 0; j < yDim; j++) {
            (*field)[i][j] = new glm::vec3[zDim];
        }
    }
    maxIndexes = glm::vec3(xDim, yDim, zDim);

    //Find the BoundingBox
    float xmin(1.0f), ymin(1.0f), zmin(1.0f);
    float xmax(-1.0f), ymax(-1.0f), zmax(-1.0f);
    sscanf(FindAndJump(buffer, "BoundingBox"), "%g %g %g %g %g %g", &xmin, &xmax, &ymin, &ymax, &zmin, &zmax);
    printf("\tBoundingBox in x-Direction: [%g ... %g]\n", xmin, xmax);
    printf("\tBoundingBox in y-Direction: [%g ... %g]\n", ymin, ymax);
    printf("\tBoundingBox in z-Direction: [%g ... %g]\n", zmin, zmax);
    bmin = glm::vec3(xmin, zmin, ymin);
    bmax = glm::vec3(xmax, zmax, ymax);
    fieldSize = glm::abs(bmax - bmin);
    resolution = glm::vec3(fieldSize.x/xDim, fieldSize.y/zDim, fieldSize.z/yDim);
    if(timeDep) {
        startTime = zmin;
        flowTime = startTime;
    }
    
    //Is it a uniform grid? We need this only for the sanity check below.
    const bool bIsUniform = (strstr(buffer, "CoordType \"uniform\"") != NULL);
    printf("\tGridType: %s\n", bIsUniform ? "uniform" : "UNKNOWN");

    //Type of the field: scalar, vector
    int NumComponents(0);
    if (strstr(buffer, "Lattice { float Data }"))
    {
        //Scalar field
        NumComponents = 1;
    }
    else
    {
        //A field with more than one component, i.e., a vector field
        sscanf(FindAndJump(buffer, "Lattice { float["), "%d", &NumComponents);
    }
    printf("\tNumber of Components: %d\n", NumComponents);

    //Sanity check
    if (xDim <= 0 || yDim <= 0 || zDim <= 0
        || xmin > xmax || ymin > ymax || zmin > zmax
        || !bIsUniform || NumComponents <= 0)
    {
        printf("Something went wrong\n");
        fclose(fp);
        return;
    }

    //Find the beginning of the data section
    const long idxStartData = strstr(buffer, "# Data section follows") - buffer;
    if (idxStartData > 0)
    {
        //Set the file pointer to the beginning of "# Data section follows"
        fseek(fp, idxStartData, SEEK_SET);
        //Consume this line, which is "# Data section follows"
        fgets(buffer, 2047, fp);
        //Consume the next line, which is "@1"
        fgets(buffer, 2047, fp);

        //Read the data
        // - how much to read
        const size_t NumToRead = xDim * yDim * zDim * NumComponents;
        // - prepare memory; use malloc() if you're using pure C
        float* pData = new float[NumToRead];
        if (pData)
        {
            // - do it
            const size_t ActRead = fread((void*)pData, sizeof(float), NumToRead, fp);
            // - ok?
            if (NumToRead != ActRead)
            {
                printf("Something went wrong while reading the binary data section.\nPremature end of file?\n");
                delete[] pData;
                fclose(fp);
                return;
            }

            //Test: Print all data values
            //Note: Data runs x-fastest, i.e., the loop over the x-axis is the innermost
            //printf("\nPrinting all values in the same order in which they are in memory:\n");
            int Idx(0);
            for(int k=0;k<zDim;k++)
            {
                for(int j=0;j<yDim;j++)
                {
                    for(int i=0;i<xDim;i++)
                    {
                        //Note: Random access to the value (of the first component) of the grid point (i,j,k):
                        // pData[((k * yDim + j) * xDim + i) * NumComponents]
                        assert(pData[((k * yDim + j) * xDim + i) * NumComponents] == pData[Idx * NumComponents]);

                        //for(int c=0;c<NumComponents;c++)
                        //{
                            //printf("%g ", pData[Idx * NumComponents + c]);
                        //}
                        //printf("\n");
                        if(NumComponents == 1) {
                            (*field)[i][j][k] = glm::vec3(pData[Idx*2], 0.0f, 0.0f);
                        }
                        else if(NumComponents == 2) {
                            (*field)[i][j][k] = glm::vec3(pData[Idx*2], 0.0f, pData[Idx*2+1]);
                            //printf("Vector: (%f, %f, %f)\n", (*field)[i][j][k].x, (*field)[i][j][k].y, (*field)[i][j][k].z);
                        }
                        else if(NumComponents == 3) {
                            //y,z components switched
                            (*field)[i][j][k] = glm::vec3(pData[Idx*3], pData[Idx*3+2], pData[Idx*3+1]);
                            //printf("Vector: (%f, %f, %f)\n", (*field)[i][j][k].x, (*field)[i][j][k].y, (*field)[i][j][k].z);
                        }
                        Idx++;
                    }
                }
            }

            delete[] pData;
        }
    }

    fclose(fp);
}
