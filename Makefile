SDKDIR=/Users/alex/OculusSDK2
OS := $(shell uname)
ifeq ($(OS), Darwin)
CC=/usr/local/bin/g++-4.9
LFLAGS=-framework SDL2 -framework OpenGL -lGLEW
else
CC=g++
LFLAGS=-lSDL2 -lGL -lGLEW
endif
CFLAGS=-std=c++11 -g -Wall -fopenmp
DFLAG=
ifdef DEBUG
	DFLAG+=-DDEBUG=1
endif
INCLUDES=-I /opt/X11/include/GL/
ifdef RELEASE
	FRAMEWORKS+=-F$(SDKDIR)/LibOVR/Lib/Mac/Release
else
	FRAMEWORKS+=-F$(SDKDIR)/LibOVR/Lib/Mac/Debug
endif

all: vecfield ORvecfield

vecfield: vecfield.o Particle.o
	$(CC) $(INCLUDES) $^ $(CFLAGS) $(LFLAGS) $(DFLAG) -o $@

Particle.o: Particle.cpp
	$(CC) $(CFLAGS) $(DFLAG) -c $^

vecfield.o: vecfield.cpp
	$(CC) $(CFLAGS) $(DFLAG) -c $^

ORvecfield: ORvecfield.o Particle.o
	$(CC) $(INCLUDES) $(FRAMEWORKS) $^ $(CFLAGS) $(LFLAGS) -framework LibOVR $(DFLAG) -o $@

ORvecfield.o: ORvecfield.cpp
	$(CC) $(INCLUDES) -I $(SDKDIR)/LibOVR/Include/ $(CFLAGS) $(DFLAG) -c $^

clean:
	rm -rf *.o vecfield ORvecfield