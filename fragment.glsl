#version 140

in vec3 fColor;
in vec3 normal;
in vec3 pos;
in vec3 lightDir;

//layout(location = 0) out vec4 outColor;
//layout(location = 1) out vec4 outColor2;
out vec4 outColor;

const float ambient = 0.5f;
void main() {
    float r = dot(gl_PointCoord-0.5, gl_PointCoord-0.5);
    if(r > 0.25) {
        discard;
    }
    else {
        float alpha = 1.0f - (2.0 * sqrt(r));
        vec3 color = fColor;
        vec3 diffuseC = color*max(dot(-lightDir,normal),0.0);
        vec3 ambC = color*ambient;
        vec3 viewDir = normalize(-pos); //We know the eye is at (0,0)!
        vec3 reflectDir = reflect(viewDir,normal);
        float spec = max(dot(reflectDir,lightDir),0.0);
        if (dot(-lightDir,normal) <= 0.0)spec = 0;
        vec3 specC = vec3(1.0,1.0,1.0)*pow(spec,4);
        outColor = vec4(ambC+diffuseC+specC, alpha);
        //outColor2 = outColor;
    }
}
